/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpd_visualisation.layout.test;

/**
 *
 * @author linckomac
 */
import java.util.List;
import java.util.Random;

import jpd_visualisation.graph.Vertex;
import jpd_visualisation.graph.Graph;
import jpd_visualisation.layout.base.Layout;

public class RandomLayout extends Layout {

    Graph graph;

    Random rnd = new Random();

    public RandomLayout(Graph graph) {

        this.graph = graph;

    }

    @Override
    public boolean execute() { return false;}

    @Override
    public void initialize() {
        List<Vertex> vertex = graph.getModel().getAllVertices();

        vertex.stream().forEach((v) -> {
            double x = rnd.nextDouble() * 500;
            double y = rnd.nextDouble() * 500;

            v.relocate(x, y);
        });
    }

}