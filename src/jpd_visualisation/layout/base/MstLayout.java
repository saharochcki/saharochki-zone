package jpd_visualisation.layout.base;

import jpd_visualisation.graph.Edge;
import jpd_visualisation.graph.EdgeComparator;
import jpd_visualisation.graph.Graph;
import jpd_visualisation.graph.Vertex;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;


public class MstLayout extends Layout  {
	protected Graph graph;
	protected PriorityQueue<Edge> queue;

	public MstLayout(Graph graph) {
		this.graph = graph;
		Comparator<Edge> comparator = new EdgeComparator();
		this.queue = new PriorityQueue<Edge>(4, comparator);
	}

	protected void keepConnections(Vertex vertex) {
		List<Edge> edge = this.graph.getModel().getAllEdges();
		vertex.setMark(true);
		edge.stream().forEach((e) -> {
			if (isValidEdge(e))
				this.queue.add(e);
		});
	}

	protected boolean isValidEdge(Edge edge) {
		return ( !edge.isMarked() && ( (edge.getTarget().isMarked()) ^ (edge.getSource().isMarked())) );
	}

	@Override
	public void initialize() {
		queue.clear();
		Vertex keep = graph.getModel().getAllVertices().get(0);
		keepConnections(keep);
		List<Edge> edge = graph.getModel().getAllEdges();
		List<Vertex> vertex = graph.getModel().getAllVertices();
		edge.stream().forEach((e) -> {
			e.setMark(false);
		});

		vertex.stream().forEach((v) -> {
			v.setMark(false);
		});
		keep.setMark(true);
		}


	@Override
	public boolean execute() {
		Edge top;
		do {
			if (this.queue.isEmpty()) {
				//fin
				return false;
			}
		}  while ( !isValidEdge(top = this.queue.remove()) );
		top.setMark(true);
		keepConnections(top.getUnmarked());
		return true;
	};
}
