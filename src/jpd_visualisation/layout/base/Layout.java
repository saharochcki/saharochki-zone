/**
 * @author
 */

package jpd_visualisation.layout.base;

public abstract class Layout {

    public abstract boolean execute();
    public abstract void initialize();

}