package jpd_visualisation;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import jpd_visualisation.graph.BoopBar;
import jpd_visualisation.graph.Edge;
import jpd_visualisation.graph.Graph;
import jpd_visualisation.graph.Model;
import jpd_visualisation.layout.base.MstLayout;
import jpd_visualisation.layout.test.RandomLayout;

import java.util.Random;

/**
 *
 * @author Бибакова, Панченко, Шагров и, конечно же, Андрей -xxx-Maser=X=of=X=Darcnes-xxx- Базукеров
 */
public class jpd_visualisation extends Application {

    Graph graph = new Graph();

    @Override
    public void start(Stage primaryStage) {
        BorderPane root = new BorderPane();

        graph = new Graph();

        root.setCenter(graph.getScrollPane());

        Scene scene = new Scene(root, 640, 480);
        scene.getStylesheets().add(getClass().getResource("JPD_visualisation.css").toExternalForm());
        primaryStage.setScene(scene);

		/*
		HBox taskbar = new HBox(10);
	//	taskbar.setPadding(new Insets(10, 30, 50, 30));
		taskbar.setPrefHeight(50);
	//	taskbar.setAlignment(Pos.CENTER);
		root.setBottom(taskbar);

		Button stepButton = new Button("      Шаг      ");
		Button startButton = new Button("     Старт     ");*/
		Button vertexButton = new Button("Add vertex");
		Button edgeButton = new Button(" Add edge ");

		/*startButton.setOnAction(event -> {
			//начало
		});

		stepButton.setOnAction(event -> {
			//шаг
		});*/

		vertexButton.setOnAction(event -> {
			int n = graph.getModel().getAllVertices().size();
			Random random = new Random();
			int weight = random.nextInt(25) + 1;
			int v1 = random.nextInt(n);

			graph.beginUpdate();

			graph.getModel().addVertex(Integer.toString(n));

			graph.getModel().addEdge(graph.getModel().getAllVertices().get(v1).getVertexId(), Integer.toString(n), weight);

			graph.endUpdate();

		});

		edgeButton.setOnAction(event -> {
			int m = graph.getModel().getAllEdges().size();
			int n = graph.getModel().getAllVertices().size();
			if (m < (n*(n-1)/2))
			{
				Random random = new Random();
				int weight = random.nextInt(25) + 1;
				Edge edge;
				int v1, v2;
				do
				{
					v1 = random.nextInt(n);
					v2 = v1;
					while (v1 == v2) v2 = random.nextInt(n);
					edge = new Edge(graph.getModel().getAllVertices().get(v1), graph.getModel().getAllVertices().get(v2), weight);
				} while (graph.getModel().hasEdge(edge));

				graph.beginUpdate();

				graph.getModel().addEdge(graph.getModel().getAllVertices().get(v1).getVertexId(), graph.getModel().getAllVertices().get(v2).getVertexId(), weight);

				graph.endUpdate();
			}

		});

		//taskbar.getChildren().addAll(startButton, stepButton, vertexButton, edgeButton);


		addGraphComponents();

		BoopBar boopBar = new BoopBar();
		MstLayout mstLayout = new MstLayout(graph);
		boopBar.addBooper("Start", "Continue", mstLayout);
		boopBar.getChildren().addAll(vertexButton, edgeButton);
		root.setBottom(boopBar);

        RandomLayout layout = new RandomLayout(graph);

		primaryStage.show();

        layout.initialize();
    }

    private void addGraphComponents() {

        Model model = graph.getModel();

        graph.beginUpdate();

        model.addVertex("V_0");
        model.addVertex("V_1");
        model.addVertex("V_2");

        model.addEdge("V_0", "V_1", 1);
        model.addEdge("V_2", "V_1", 3);
        model.addEdge("V_0", "V_2", 2);
        
        graph.endUpdate();

    }

    public static void main(String[] args) {
        launch(args);
    }

}

// Было Начало, и тогда потребовалось Существование, потому появился Космос.