package jpd_visualisation.graph;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import jpd_visualisation.layout.base.Layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class BoopBar extends HBox {

	protected class Booper extends Button {

		protected boolean active;
		protected Layout layout;

		String caption;
		String activeCaption;

		public Booper(String caption, String activeCaption, Layout layout) {
			super(caption);
			this.caption = caption;
			this.activeCaption = activeCaption;
			this.active  = false;
			this.layout = layout;
			setOnMousePressed(booperEventHandler);
		}

		public void performTask() {
			if (!this.active) {
				this.active = true;
				this.setText(this.activeCaption);
				this.layout.initialize();
				return;
			}
			if (!this.layout.execute()) endTask();
		}

		public boolean isActive() { return this.active; }

		public void endTask() {
			this.active = false;
			this.setText(this.caption);
		}
	}

	public BoopBar() { super(); setPrefHeight(50);}

	public void addBooper(String caption, String activeCaption, Layout layout) {
		Booper booper = new Booper(caption, activeCaption, layout);
		getChildren().add(booper);
	}

	EventHandler<MouseEvent> booperEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {

			Booper booper = (Booper) event.getSource();
			booper.performTask();

		}
	};



}
