/**
 * @author Бон-Батон
 */

package jpd_visualisation.graph;

import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;

public class Graph {

    private Model model;

    private Group canvas;

    private ZoomableScrollPane scrollPane;


    MouseGestures mouseGestures;

    VertexLayer vertexLayer;

    public Graph() {

        this.model = new Model();

        canvas = new Group();
        vertexLayer = new VertexLayer();

        canvas.getChildren().add(vertexLayer);

        mouseGestures = new MouseGestures(this);

        scrollPane = new ZoomableScrollPane(canvas);

        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);

    }

    public ScrollPane getScrollPane() {
        return this.scrollPane;
    }

    public Pane getVertexLayer() {
        return this.vertexLayer;
    }

    public Model getModel() {
        return model;
    }

    public void beginUpdate() {}

    public void endUpdate() {

        getVertexLayer().getChildren().addAll(model.getAddedEdges());
        getVertexLayer().getChildren().addAll(model.getAddedVertices());

        getVertexLayer().getChildren().removeAll(model.getRemovedVertices());
        getVertexLayer().getChildren().removeAll(model.getRemovedEdges());

        for (Vertex vertex : model.getAddedVertices()) {
            mouseGestures.makeDraggable(vertex);
        }

        getModel().attachOrphansToGraphParent(model.getAddedVertices());

        getModel().disconnectFromGraphParent(model.getRemovedVertices());

        getModel().merge();

    }

    public double getScale() {
        return this.scrollPane.getScaleValue();
    }

}