package jpd_visualisation.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {

    Vertex graphParent;

    List<Vertex> allVertices;
    List<Vertex> addedVertices;
    List<Vertex> removedVertices;

    List<Edge> allEdges;
    List<Edge> addedEdges;
    List<Edge> removedEdges;

    Map<String, Vertex> vertexMap;

    public Model() {

        graphParent = new Vertex("_ROOT_");
        clear();
    }

    public void clear() {

        allVertices = new ArrayList<>();
        addedVertices = new ArrayList<>();
        removedVertices = new ArrayList<>();

        allEdges = new ArrayList<>();
        addedEdges = new ArrayList<>();
        removedEdges = new ArrayList<>();

        vertexMap = new HashMap<>();

    }

    public void clearAddedLists() {
        addedVertices.clear();
        addedEdges.clear();
    }

    public List<Vertex> getAddedVertices() {
        return addedVertices;
    }

    public List<Vertex> getRemovedVertices() {
        return removedVertices;
    }

    public List<Vertex> getAllVertices() {
        return allVertices;
    }

    public List<Edge> getAddedEdges() {
        return addedEdges;
    }

    public List<Edge> getRemovedEdges() {
        return removedEdges;
    }

    public List<Edge> getAllEdges() {
        return allEdges;
    }

    public void addVertex(Vertex vertex) {
        addedVertices.add(vertex);
        vertexMap.put(vertex.getVertexId(), vertex);
    }

    public void addVertex(String id) {
        Vertex vertex = new Vertex(id);
        addVertex(vertex);
    }


    public void addEdge(String sourceId, String targetId, int weight) {

        Vertex sourceVertex = vertexMap.get(sourceId);
        Vertex targetVertex = vertexMap.get(targetId);

        Edge edge = new Edge(sourceVertex, targetVertex, weight);

        addedEdges.add(edge);

    }

    /**
     * Attach all vertices which don't have a parent to graphParent
     *
     * @param vertexList
     */
    public void attachOrphansToGraphParent(List<Vertex> vertexList) {

        for (Vertex vertex : vertexList) {
            if (vertex.getVertexParents().size() == 0) {
                graphParent.addVertexChild(vertex);
            }
        }

    }

    /**
     * Remove the graphParent reference if it is set
     *
     * @param vertexList
     */
    public void disconnectFromGraphParent(List<Vertex> vertexList) {

        for (Vertex vertex : vertexList) {
            graphParent.removeVertexChild(vertex);
        }
    }

    public void merge() {

        allVertices.addAll(addedVertices);
        allVertices.removeAll(removedVertices);

        addedVertices.clear();
        removedVertices.clear();

        allEdges.addAll(addedEdges);
        allEdges.removeAll(removedEdges);

        addedEdges.clear();
        removedEdges.clear();

    }

    public boolean hasEdge(Edge edge) {
        for (Edge e: allEdges) {
            if (( (edge.source == e.source) && (edge.target == e.target) ) || ( (edge.source == e.target) && (edge.target == e.source) )) return true;
        }
        return false;
    }


}