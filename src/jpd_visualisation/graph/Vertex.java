/**
 *
 * @author Парень со двора
 */

package jpd_visualisation.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.layout.Pane;

public class Vertex extends Pane {

    String vertexId;
    protected boolean mark;

    List<Vertex> children = new ArrayList<>();
    List<Vertex> parents = new ArrayList<>();

    Node view;

    public Vertex(String vertexId) {
        this.vertexId = vertexId;
        setMark(false);
    }

    public boolean isMarked() { return mark; }

    public void addVertexChild(Vertex vertex) {
        children.add(vertex);
    }

    public List<Vertex> getVertexChildren() {
        return children;
    }

    public void addVertexParent(Vertex vertex) {
        parents.add(vertex);
    }

    public List<Vertex> getVertexParents() {
        return parents;
    }

    public void removeVertexChild(Vertex vertex) {
        children.remove(vertex);
    }

    public void setView(Node view) {
        getChildren().remove(this.view);
        this.view = view;
        getChildren().add(view);

    }

    public void setView(int r, Color c1, Color c2) {
        Circle view = new Circle(r);
        view.setStroke(c1);
        view.setFill(c2);
        setView(view);
    }

    public Node getView() {
        return this.view;
    }

    public void setMark(boolean m) {
        this.mark = m;
        if (!m) {
           setView(10, Color.BLACK, Color.CADETBLUE);
        }
        else {
            setView(13, Color.DARKRED, Color.DEEPPINK);
        }
        //this.view = view;
        //setView(view);
    }

    public String getVertexId() {
        return vertexId;
    }
}

// Желал своей картине Космос Движения, и было Течение,
// когда потребовал Космос Перемен, и появилось Время,
// а с ним Космос оживил Краски: Небо осветило Воду и Землю чередованием звёзд и солнца, ,
// Вода по Земле растеклась реками и разлилась дождями, 
// а из Земли явились согретые и сытые Существа...