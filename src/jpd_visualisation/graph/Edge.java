package jpd_visualisation.graph;

/**
 * @author бутерброд с сыром
 */

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import java.util.Comparator;

public class Edge extends Group {

    protected Vertex source;
    protected Vertex target;
    protected int weight;
    protected boolean mark;

    Line line;

    public Edge(Vertex source, Vertex target, int weight) {

        this.source = source;
        this.target = target;
        this.weight = weight;

        source.addVertexChild(target);
        target.addVertexParent(source);

        line = new Line();

        line.startXProperty().bind(source.layoutXProperty());//.add(source.getBoundsInParent().getWidth() / 4));
        line.startYProperty().bind(source.layoutYProperty());//.add(source.getBoundsInParent().getHeight() / 4));

        line.endXProperty().bind(target.layoutXProperty());//.add( target.getBoundsInParent().getWidth() / 4) );
        line.endYProperty().bind(target.layoutYProperty());//.add( target.getBoundsInParent().getHeight() / 4));

        getChildren().add(line);

        Text label = new Text(String.valueOf(this.weight));

        label.xProperty().bind((line.startXProperty().add(line.endXProperty())).divide(2));
        label.yProperty().bind((line.startYProperty().add(line.endYProperty())).divide(2));

        getChildren().add(label);
        setMark(false);

    }

    public void setMark(boolean mark) {
        this.mark = mark;
        if (mark) line.setStrokeWidth(5);
             else line.setStrokeWidth(1);
    }

    public boolean isMarked() { return this.mark;}

    public Vertex getUnmarked() {
        if (this.source.isMarked()==this.target.isMarked()) return null;
        if (this.source.isMarked())return this.target; else return this.source;
    }

    public void setWeight(int a) { this.weight = a; }

    public int getWeight() { return this.weight; }

    public Vertex getSource() {
        return source;
    }

    public Vertex getTarget() {
        return target;
    }

}

